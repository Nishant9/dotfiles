
;;Proxy
(setq url-proxy-services
   '(("no_proxy" . "^\\(localhost\\|10.*\\)")
     ("http" . "nknproxy.iitk.ac.in:3128")
     ("https" . "nknproxy.iitk.ac.in:3128")))

(setq url-http-proxy-basic-auth-storage
    (list (list "nknproxy.iitk.ac.in:3128"
                (cons "Input your LDAP UID !"
                      (base64-encode-string "cseguest:natraj25")))))

;;Proxy Ends

;;;;;Package Manager
(require 'package) ;; You might already have this line
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/")
	     '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize) ;; You might already have this line



;;;;;;;;Emacs Basic Configuration 
(setq inhibit-startup-message t)
(setq-default tab-width 2)
(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "google-chrome-stable")
(setq save-interprogram-paste-before-kill t)
(add-to-list 'auto-mode-alist '("\\.*rc$" . conf-unix-mode))
(delete-selection-mode)
(require 'smartparens-config)
(smartparens-global-mode 1)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq-default indent-tabs-mode nil)
(defalias 'yes-or-no-p 'y-or-n-p)
;;Theme

(if (daemonp)
    (add-hook 'after-make-frame-functions
        (lambda (frame)
            (select-frame frame)
            (load-theme 'material t)))
	(load-theme 'material t))

(setq default-frame-alist '((font . "PragmataPro-13")))
(defun sudo-find-file (file-name)
  "Like find file, but opens the file as root."
  (interactive "FSudo Find File: ")
  (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
    (find-file tramp-file-name)))

;;;;;;Emacs Basic Configuration Ends
(load "~/.emacs.d/init.d/better-defaults.el")
(load "~/.emacs.d/init.d/helm.el") 
(load "~/.emacs.d/init.d/haskell.el")
(load "~/.emacs.d/init.d/auto-complete.el") 
(load "~/.emacs.d/init.d/ace.el") 
;;(load "~/.emacs.d/init.d/key-chord.el") 
(load "~/.emacs.d/init.d/guide.el") 


(global-fasd-mode 1)
(define-key global-map (kbd "C-x C-d") 'fasd-find-file)

;;;;Latex
(set-default 'preview-scale-function 1.6)
;;;;;;;;;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-view-program-list (quote (("Zathura" ("zathura %o") ""))))
 '(TeX-view-program-selection (quote ((output-html "xdg-open") (nil "xdg-open"))))
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["color-237" "#d75f5f" "#afaf00" "#ffaf00" "#87afaf" "#d787af" "#87af87" "color-223"])
 '(custom-safe-themes
   (quote
    ("82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "5d1434865473463d79ee0523c1ae60ecb731ab8d134a2e6f25c17a2b497dd459" default)))
 '(fasd-completing-read-function nil)
 '(fci-rule-color "#003f8e")
 '(fringe-mode 0 nil (fringe))
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-tags-on-save t)
 '(helm-M-x-fuzzy-match t)
 '(helm-apropos-fuzzy-match t)
 '(helm-autoresize-mode t)
 '(helm-buffers-fuzzy-matching t)
 '(helm-file-cache-fuzzy-match t)
 '(helm-lisp-fuzzy-completion t)
 '(helm-locate-fuzzy-match t)
 '(helm-mode t)
 '(helm-recentf-fuzzy-match t)
 '(hindent-style "chris-done")
 '(menu-bar-mode nil)
 '(package-check-signature nil)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#ff9da4")
     (40 . "#ffc58f")
     (60 . "#ffeead")
     (80 . "#d1f1a9")
     (100 . "#99ffff")
     (120 . "#bbdaff")
     (140 . "#ebbbff")
     (160 . "#ff9da4")
     (180 . "#ffc58f")
     (200 . "#ffeead")
     (220 . "#d1f1a9")
     (240 . "#99ffff")
     (260 . "#bbdaff")
     (280 . "#ebbbff")
     (300 . "#ff9da4")
     (320 . "#ffc58f")
     (340 . "#ffeead")
     (360 . "#d1f1a9"))))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "PragmataPro" :foundry "unknown" :slant normal :weight normal :height 110 :width normal))))
 '(helm-ff-dotted-directory ((t (:foreground "gainsboro"))))
 '(helm-source-header ((t (:foreground "deep sky blue" :weight bold :height 1.2)))))
