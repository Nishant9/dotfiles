;;;;;Company
(add-hook 'after-init-hook 'global-company-mode)
(with-eval-after-load 'company
(let ((map company-active-map))
  (define-key map (kbd "C-/") 'company-search-candidates)
  (define-key map (kbd "C-M-/") 'company-filter-candidates)
  (define-key map (kbd "C-d") 'company-show-doc-buffer)
  (define-key map (kbd "C-j") 'company-select-next)
  (define-key map (kbd "C-k") 'company-select-previous)
  (define-key map (kbd "C-l") 'company-complete-selection))
(add-to-list 'company-backends 'company-files)
(delete 'company-backends 'company-css))

(setq hippie-expand-try-functions-list
      '(
	;; Try to expand word "dynamically", searching the current buffer.
	try-expand-dabbrev
	;; Try to expand word "dynamically", searching all other buffers.
	try-expand-dabbrev-all-buffers
	;; Try to expand word "dynamically", searching the kill ring.
	try-expand-dabbrev-from-kill
	;; Try to complete text as a file name, as many characters as unique.
	try-complete-file-name-partially
	;; Try to complete text as a file name.
	try-complete-file-name
	;; Try to expand word before point according to all abbrev tables.
	try-expand-all-abbrevs
	;; Try to complete the current line to an entire line in the buffer.
	try-expand-list
	;; Try to complete the current line to an entire line in the buffer.
	try-expand-line
	;; Try to complete as an Emacs Lisp symbol, as many characters as
	;; unique.
	try-complete-lisp-symbol-partially
	;; Try to complete word as an Emacs Lisp symbol.
	try-complete-lisp-symbol))
;;;;;;;Company Ends
