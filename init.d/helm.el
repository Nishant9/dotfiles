;;;;Helm
(require 'helm)
(require 'helm-config)
;; fuzzy matching setting
(helm-autoresize-mode 1)
(setq helm-M-x-fuzzy-match t
      helm-apropos-fuzzy-match t
      helm-file-cache-fuzzy-match t
      helm-imenu-fuzzy-match t
      helm-lisp-fuzzy-completion t
      helm-locate-fuzzy-match t
      helm-recentf-fuzzy-match t
      helm-semantic-fuzzy-match t
      helm-buffers-fuzzy-matching t)
(define-key helm-map (kbd "C-j") 'helm-next-line)
(define-key helm-map (kbd "C-k") 'helm-previous-line)
(define-key helm-map (kbd "C-h") 'helm-next-source)
(define-key helm-map (kbd "C-l") 'helm-previous-source)
;; Swap default TAB and C-z commands.
;; For GUI.
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
;; For terminal.
(define-key helm-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") 'helm-select-action)
(define-key global-map (kbd "C-x C-m") 'helm-M-x)
(define-key global-map (kbd "C-x C-f") 'helm-find-files)
(helm-mode 1)
;;;;Helm ends
