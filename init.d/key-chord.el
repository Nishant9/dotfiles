;;;;;Key Chord
;;
(require 'key-chord)
(key-chord-mode 1)
;;
;; and some chords, for example
;;
(key-chord-define-global "fd"     'keyboard-escape-quit)
;;;;;Key Chord Ends
